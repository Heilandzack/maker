module gitlab.com/crankykernel/maker/go

require (
	github.com/crankykernel/binanceapi-go v0.0.0-20190318152857-632f0d98fabb
	github.com/gobuffalo/buffalo-plugins v1.13.0 // indirect
	github.com/gobuffalo/genny v0.0.0-20190315124720-e16e52a93c79 // indirect
	github.com/gobuffalo/meta v0.0.0-20190207205153-50a99e08b8cf // indirect
	github.com/gobuffalo/packr/v2 v2.6.0
	github.com/gobuffalo/syncx v0.0.0-20190224160051-33c29581e754 // indirect
	github.com/gorilla/context v1.1.1 // indirect
	github.com/gorilla/mux v1.6.2
	github.com/gorilla/websocket v1.4.0
	github.com/inconshreveable/mousetrap v1.0.0
	github.com/kr/pty v1.1.3 // indirect
	github.com/markbates/deplist v1.0.5 // indirect
	github.com/mattn/go-sqlite3 v1.9.0
	github.com/mholt/certmagic v0.0.0-20190225061201-e3e89d1096d7
	github.com/oklog/ulid v0.3.0
	github.com/sirupsen/logrus v1.4.2
	github.com/spf13/afero v1.2.1 // indirect
	github.com/spf13/cobra v0.0.5
	github.com/spf13/viper v1.3.2
	github.com/stretchr/testify v1.3.0
	golang.org/x/crypto v0.0.0-20190621222207-cc06ce4a13d4
	golang.org/x/text v0.3.1-0.20180807135948-17ff2d5776d2 // indirect
	gopkg.in/yaml.v2 v2.2.2
)

//replace github.com/crankykernel/binanceapi-go => ../../binanceapi-go
