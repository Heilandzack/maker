// The MIT License (MIT)
//
// Copyright (c) 2018 Cranky Kernel
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package types

import (
	"github.com/crankykernel/binanceapi-go"
	"time"
)

type TradeStateV0 struct {
	Version int64

	// Trade ID local to this app. Its actually a ULID, but saved as a string.
	LocalID string

	Symbol    string
	OpenTime  time.Time
	CloseTime *time.Time `json:",omitempty"`
	Status    TradeStatus
	Fee       float64

	BuyOrderId int64

	ClientOrderIDs map[string]bool

	BuyOrder struct {
		Quantity float64
		Price    float64
	}

	BuySideFills    []OrderFill `json:",omitempty"`
	BuyFillQuantity float64

	// The average buy price per unit not accounting for fees.
	AverageBuyPrice float64

	// The total cost of the buy, including fees.
	BuyCost float64

	// The buy price per unit accounting for fees.
	EffectiveBuyPrice float64

	SellOrderId int64

	SellSideFills    []OrderFill `json:",omitempty"`
	SellFillQuantity float64
	AverageSellPrice float64
	SellCost         float64

	StopLoss struct {
		Enabled   bool
		Percent   float64
		Triggered bool
	}

	LimitSell struct {
		Enabled bool
		Percent float64
	}

	TrailingProfit struct {
		Enabled   bool
		Percent   float64
		Deviation float64
		Activated bool
		Price     float64
		// Set to true when the sell order has been sent.
		Triggered bool
	}

	// The profit in units of the quote asset.
	Profit float64

	// The profit as a percentage (0-100).
	ProfitPercent float64

	LastBuyStatus binanceapi.OrderStatus

	SellOrder struct {
		Status   binanceapi.OrderStatus
		Type     string
		Quantity float64
		Price    float64
	}

	// The last known price for this symbol. Use to estimate profit. Source may
	// not always be the last price, but could also be the last best bid or ask.
	LastPrice float64
}
