// The MIT License (MIT)
//
// Copyright (c) 2018 Cranky Kernel
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import {Component, ElementRef, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import * as $ from "jquery";
import {MakerService, TradeState, TradeStatus} from '../maker.service';

@Component({
    selector: 'app-trailingprofitform',
    templateUrl: './trailing-profit-form.component.html',
    styleUrls: ['./trailing-profit-form.component.scss']
})
export class TrailingProfitFormComponent implements OnInit {

    TradeStatus = TradeStatus;

    @Input() trade: TradeState = null;

    form: FormGroup;

    constructor(private fb: FormBuilder,
                private el: ElementRef,
                private maker: MakerService) {
    }

    ngOnInit() {
        this.buildForm();
        if (this.trade.Status === TradeStatus.DONE) {
            $(this.el.nativeElement).find("input").attr("disabled", "disabled");
        }
    }

    private buildForm() {
        if (!this.trade.TrailingProfit) {
            this.trade.TrailingProfit.Enabled = false;
            this.trade.TrailingProfit.Percent = 0;
            this.trade.TrailingProfit.Deviation = 0;
        }
        this.form = this.fb.group({
            enabled: [this.trade.TrailingProfit.Enabled,],
            percent: [this.trade.TrailingProfit.Percent, Validators.required,],
            deviation: [this.trade.TrailingProfit.Deviation, Validators.required,],
        })
    }

    onSubmit() {
        const formModel: FormModel = this.form.value;
        this.maker.updateTrailingProfit(this.trade, formModel.enabled,
                +formModel.percent, +formModel.deviation);
    }

    reset() {
        this.buildForm();
    }
}

interface FormModel {
    enabled: boolean;
    percent: number;
    deviation: number,
}
