// The MIT License (MIT)
//
// Copyright (c) 2018 Cranky Kernel
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MakerService, TradeState, TradeStatus} from '../maker.service';

@Component({
    selector: 'app-stoploss-form',
    templateUrl: './stop-loss-form.component.html',
    styleUrls: ['./stop-loss-form.component.scss']
})
export class StopLossFormComponent implements OnInit {

    TradeStatus = TradeStatus;

    @Input() trade: TradeState = null;

    form: FormGroup;

    constructor(private fb: FormBuilder,
                private maker: MakerService) {
    }

    ngOnInit() {
        this.buildForm();
    }

    private buildForm() {
        this.form = this.fb.group({
            enabled: [this.trade.StopLoss.Enabled,],
            percent: [this.trade.StopLoss.Percent,],
        });
    }

    onSubmit() {
        const formModel: FormModel = this.form.value;
        this.maker.updateStopLoss(this.trade,
                formModel.enabled, +formModel.percent);
    }

    reset() {
        this.buildForm();
    }
}

interface FormModel {
    enabled: boolean;
    percent: number;
}